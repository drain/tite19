﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public string gName;
    public int upgradeLvl;
    public int upgradeCost;

    public int dmg;
    public Transform owner;
    public int bulletSpeed;
    public GameObject bulletPrefab;

    public int reloadTime;
    public int magazineAmount;
    public int currentMagAmount;
    public int ammoAmount;

    private bool reloading;

    private void Awake()
    {
        owner = transform;
        currentMagAmount = magazineAmount;
    }

    public void ShootBullet()
    {
        if (reloading)
            return;

        if (currentMagAmount < 1 && owner.GetComponent<Movement>())
        {
            TextLog.PrintToLog("Magazine is empty.");
            return;
        }
        else if(currentMagAmount < 1 && owner.GetComponent<Enemy>())
        {
            ReloadGun();
            return;
        }

        GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
        if (owner.gameObject.layer == 8)
            bullet.layer = 8;
        else
            bullet.layer = 20;

        bullet.GetComponent<Bullet>().dmg = dmg;
        bullet.GetComponent<Bullet>().owner = transform;

        if(owner.GetComponent<Movement>())
        {
            if (GetComponent<Movement>().lastTurnKey == KeyCode.D)
                bullet.GetComponent<Rigidbody2D>().AddForce(transform.right * bulletSpeed);
            else if (GetComponent<Movement>().lastTurnKey == KeyCode.A)
                bullet.GetComponent<Rigidbody2D>().AddForce(-transform.right * bulletSpeed);
        }
        else if(owner.GetComponent<Enemy>())
        {
            if(transform.localScale.x > 0)
                bullet.GetComponent<Rigidbody2D>().AddForce(transform.right * bulletSpeed);
            else
                bullet.GetComponent<Rigidbody2D>().AddForce(-transform.right * bulletSpeed);
        }


        currentMagAmount--;
    }

    public void ReloadGun()
    {
        if(!reloading)
        {
            reloading = true;

            if (ammoAmount <= 0 || currentMagAmount == magazineAmount)
            {
                reloading = false;
                return;
            }
                
            StartCoroutine(reload());
        }
    }

    private IEnumerator reload()
    {
        yield return new WaitForSeconds(reloadTime);

        int neededAmmo = magazineAmount - currentMagAmount;

        if(ammoAmount - neededAmmo <= 0)
        {
            currentMagAmount += ammoAmount;
            ammoAmount = 0;
            TextLog.PrintToLog("Ammo reserves depleted.");
        }
        else
        {
            currentMagAmount = magazineAmount;
            ammoAmount -= neededAmmo;
        }

        reloading = false;
        if(owner.GetComponent<Movement>())
            TextLog.PrintToLog("Reloading complete.");
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.E) && owner.GetComponent<Movement>())
        {
            ShootBullet();
        }

        if (Input.GetKeyDown(KeyCode.R) && owner.GetComponent<Movement>() && !reloading)
        {
            ReloadGun();
        }
    }

}
