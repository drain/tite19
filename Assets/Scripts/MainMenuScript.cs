﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuScript : MonoBehaviour
{
    public void LoadLvl(string lvl)
    {
        Application.LoadLevel(lvl);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            LoadLvl("lvl2");
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            LoadLvl("lvl1");
        }
    }
}
