﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public string eName;
    public int health;
    public int damage;

    public float moveSpeed;
    public bool isStationary;

    public float agroRange;

    public float meleeRange;

    public bool isAffectedByGravity;

    public LayerMask mask;

    private Movement player;

    public bool isDead;

    private bool canMelee = true;
    private bool canShoot = true;

    private void Update()
    {
        if(player == null)
            player = FindObjectOfType<Movement>();
        if(!isDead)
            DoLogic();
    }

    public void TakeDmg(int dmg, float force)
    {
        health -= dmg;
        GetComponent<Rigidbody2D>().AddForce(player.transform.position - transform.position * force);

        if(health <= 0)
        {
            TextLog.PrintToLog(eName + " died..");
            isDead = true;
            //TODO: play particles and kill it
            Destroy(this.gameObject);
        }
    }

    public void DoLogic()
    {
        //RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, Vector2.right, agroRange, mask);
        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, player.transform.position - transform.position, agroRange, mask);

        if (hits.Length == 0)
            return;

        //chase player
        if(hits[0].transform.GetComponent<Movement>() && Vector2.Distance(hits[0].transform.position,transform.position) < agroRange)
        {
            /*Vector3 vectorToTarget = player.transform.position - transform.position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * moveSpeed);*/

            if(player.transform.position.x > transform.position.x)
            {
                //face right
                transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
            }
            else if (player.transform.position.x < transform.position.x)
            {
                //face left
                transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
            }

            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, moveSpeed * Time.deltaTime);

            if (GetComponent<Gun>())
            {
                if (canShoot)
                    StartCoroutine(shootTimer());
            }
            else
            {
                if (Vector2.Distance(player.transform.position, transform.position) < meleeRange && canMelee)
                {
                    StartCoroutine(meleeTimer());
                }
            }
        }    
    }

    private IEnumerator meleeTimer()
    {
        canMelee = false;
        player.GetComponent<Movement>().TakeDmg(damage, 0);
        yield return new WaitForSeconds(1);
        canMelee = true;
    }

    private IEnumerator shootTimer()
    {
        canShoot = false;
        GetComponent<Gun>().ShootBullet();
        yield return new WaitForSeconds(2.5f);
        canShoot = true;
    }
}
