﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Movement : MonoBehaviour
{
    public int health;
    public int maxHealth;

    public bool allowMovement = true;
    private Vector2 m_moveDirection = Vector2.zero;
    public float speed = 5;
    public float yVelocity = 0;
    public float fallingSpeed = 10;
    public float jumpSpeed = 0;
    private bool grounded = false;

    private Rigidbody2D m_rigidbody;

    private Transform m_transform;

    public LayerMask layer;

    private bool isJumping;
    //public ParticleSystem jumpParticle;

    public bool isUnderWater;

    public KeyCode lastTurnKey = KeyCode.A;

    public int maxJumpCount;
    private int jumpCount;
    private bool doJump;

    private Animator m_anim;

    public bool Grounded()
    {
        return grounded;
    }

    public bool IsLevitating()
    {
        return isJumping;
    }
    public float GetCurrentSpeed()
    {
        return m_rigidbody.velocity.magnitude;
    }

    public float GetDirection()
    {
        return Input.GetAxis("Horizontal");
    }

    private Vector2 moveVector;

    void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_transform = transform;
        m_anim = GetComponent<Animator>();
        lastTurnKey = KeyCode.A;
    }

    private float airOutTimer;
    private float airOutMax = 10;

    void Update()
    {
        if (isUnderWater)
        {
            if (GameManager.GM.airAmount <= 0)
            {
                airOutTimer += Time.deltaTime;
                if(airOutTimer >= airOutMax)
                {
                    health -= 1;
                    airOutTimer = 0;
                    StartCoroutine(flashDmg());

                    if (health <= 0)
                        StartCoroutine(waitDeath());
                }
                    
            }
            else
            {
                GameManager.GM.airAmount -= Time.deltaTime / 2;
                GameManager.GM.airConsumed += Time.deltaTime / 2;
            }
                
        }
        else
        {
            //fill all airtanks
            if(GameManager.GM.emptyTankAmount > 0)
            {
                TextLog.PrintToLog("Empty airtanks filled.");
                GameManager.GM.airTankAmount += GameManager.GM.emptyTankAmount;
                GameManager.GM.emptyTankAmount = 0;
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                doJump = true;
            }
        }
            

        if (GameManager.GM.energyAmount <= 0)
        {
            StartCoroutine(waitDeath());
        }
        else
            GameManager.GM.energyAmount -= Time.deltaTime / 5;
        //AdjustLevitateEnergyBar();

        UpdateAnimator();

    }

    private void UpdateAnimator()
    {
        m_anim.SetFloat("speed", Mathf.Abs(m_rigidbody.velocity.x));
        m_anim.SetFloat("ySpeed", Mathf.Abs(m_rigidbody.velocity.y));
        m_anim.SetBool("isGrounded", grounded);
        m_anim.SetBool("isUnderwater", isUnderWater);
    }

    IEnumerator flashDmg()
    {
        GameManager.GM.dmgFlash.SetActive(true);
        yield return new WaitForSeconds(.05f);
        GameManager.GM.dmgFlash.SetActive(false);
    }

    public void TakeDmg(int dmg, float force)
    {
        health -= dmg;
        StartCoroutine(flashDmg());

        if (health <= 0)
        {
            StartCoroutine(waitDeath());
        }
    }

    private IEnumerator waitDeath()
    {
        allowMovement = false;
        GameManager.GM.deathScreen.SetActive(true);
        TextLog.PrintToLog("You died..");
        yield return new WaitForSeconds(2);

        allowMovement = true;
        Application.LoadLevel(Application.loadedLevel);
    }

    void FixedUpdate()
    {
        if (allowMovement)
            DoMovement();

        if (transform.position.y < GameManager.GM.waterObject.transform.position.y - 1)
        {
            isUnderWater = true;
        }
        else
        {
            isUnderWater = false;
        }

        if(Input.GetKey(KeyCode.A) && lastTurnKey != KeyCode.A || Input.GetKey(KeyCode.LeftArrow) && lastTurnKey != KeyCode.A)
        {
            transform.localScale = new Vector3(transform.localScale.x * -1,transform.localScale.y,transform.localScale.z);
            lastTurnKey = KeyCode.A;
        }
        if(Input.GetKey(KeyCode.D) && lastTurnKey != KeyCode.D || Input.GetKey(KeyCode.RightArrow) && lastTurnKey != KeyCode.D)
        {
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
            lastTurnKey = KeyCode.D;
        }

    }

    /*void OnDrawGizmos()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(m_transform.position - new Vector3(0, .25f, 0), .25f);
    }*/

    void DoMovement()
    {
        m_rigidbody.velocity = Vector2.zero;
        moveVector = m_moveDirection;

        //if (Physics2D.CircleCast(new Vector2(m_transform.position.x, m_transform.position.y - (m_transform.localScale.y / 2)), (0.2f), -Vector2.up, (0.1f), layer))
        if (Physics2D.CircleCast(m_transform.position - new Vector3(0, .28f, 0), .22f, -Vector2.up, .01f, layer))
            grounded = true;
        else
            grounded = false;

        if(!isUnderWater)
        {
            if(doJump)
            {
                doJump = false;
                if (jumpCount < maxJumpCount)
                {
                    yVelocity = jumpSpeed;
                    jumpCount++;
                    isJumping = true;
                    m_anim.SetTrigger("jumpStart");
                }
            }

            if(!grounded)
                yVelocity += Physics2D.gravity.y * fallingSpeed * Time.deltaTime;
        }
        else
        {
            if (jumpCount > 0)
                jumpCount = 0;

            if (Input.GetKey(KeyCode.Space))
            {
                yVelocity = jumpSpeed;
            }
            else
            {
                yVelocity += Physics2D.gravity.y * fallingSpeed * Time.deltaTime;
            }
        }

        if (grounded && yVelocity < 0)
        {
            yVelocity = Physics2D.gravity.y * Time.deltaTime;

            jumpCount = 0;
        }   

        moveVector.y = yVelocity * Time.deltaTime;

        m_moveDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        m_moveDirection = m_transform.TransformDirection(m_moveDirection);

        m_moveDirection *= speed;

        if(isUnderWater)
            m_rigidbody.velocity = moveVector/1.5f;
        else
            m_rigidbody.velocity = moveVector;
 
    }
}