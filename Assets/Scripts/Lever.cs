﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : MonoBehaviour
{
    public int doorOpenIndex;
    public int waterLevelIndex;

    public Sprite s1, s2;

    public void InteractWithLever()
    {
        if(doorOpenIndex != -1)
        {
            //OPEN DOOR
            GameManager.GM.OpenDoor(doorOpenIndex);
            TextLog.PrintToLog("Door opened somewhere..");
            //TODO: Change visual?
        }

        if(waterLevelIndex != -1)
        {
            //CHANGE WATER LEVEl
            GameManager.GM.ChangeWaterLevel(waterLevelIndex);
            TextLog.PrintToLog("Water level changed..");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<Movement>())
        {
            InteractWithLever();
            GetComponent<SpriteRenderer>().sprite = s2;
            Destroy(this);
        }
    }
}
