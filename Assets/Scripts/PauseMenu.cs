﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenuStart;
    public GameObject craftingMenu;
    public GameObject journal;

    public Transform entryButtonHolder;
    public Button buttonPrefab;
    public Text entryTextView;

    public Text partsAmount;
    public Text curWepLvl;
    public Text sliderValT;
    public Slider ammoSelect;

    private bool isInMenu;
    private bool isPaused;
    
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            OpenPauseMenu();
        }
    }

    void OpenPauseMenu()
    {
        if(!isInMenu)
        {
            Time.timeScale = 0;
            pauseMenuStart.SetActive(true);
            isInMenu = true;
        }
        else
        {
            Time.timeScale = 1;
            pauseMenuStart.SetActive(false);
            journal.SetActive(false);
            craftingMenu.SetActive(false);
            isInMenu = false;
        }
    }

    public void RestartLevel()
    {
        Time.timeScale = 1;
        Application.LoadLevel(Application.loadedLevel);
    }

    public void OpenCraftingUI()
    {
        craftingMenu.SetActive(true);
        pauseMenuStart.SetActive(false);

        UpdateSlider();
        UpdateCraftingTexts();
    }

    public void UpdateCraftingTexts()
    {
        partsAmount.text = GameManager.GM.sparePartAmount.ToString();
        curWepLvl.text = "Current lvl: " + FindObjectOfType<Movement>().GetComponent<Gun>().upgradeLvl.ToString();
    }

    public void OpenJournal()
    {
        journal.SetActive(true);
        pauseMenuStart.SetActive(false);

        ClearEntryButtons();
        AddEntryButtons();
    }

    public void GoToMenu()
    {
        Time.timeScale = 1;
        Application.LoadLevel(0);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    private List<Button> bTemp = new List<Button>();

    public void ClearEntryButtons()
    {
        //Button temp;
        for (int i = 0; i < bTemp.Count; i++)
        {
            //temp = bTemp[i];
            //bTemp.Remove(temp);
            Destroy(bTemp[i].gameObject);
        }

        bTemp.Clear();
        entryTextView.text = "";
    }

    public void AddEntryButtons()
    {
        for(int i = 0; i < GameManager.collectibles.Count; i++)
        {
            Button b = Instantiate(buttonPrefab, entryButtonHolder);
            b.GetComponentInChildren<Text>().text = GameManager.collectibles[i].cName;
            Collectible c = GameManager.collectibles[i];
            b.onClick.AddListener(() => { ShowEntry(c); });
            bTemp.Add(b);
        }
    }

    public void BackToPauseMenu()
    {
        journal.SetActive(false);
        craftingMenu.SetActive(false);
        pauseMenuStart.SetActive(true);
    }

    public void ShowEntry(Collectible c)
    {
        entryTextView.text = c.cName + "\n\n" + c.lore.Replace("\\n","\n");
        //TODO: Add images?
    }

    public int ammoMaxSelectAmount;

    public void UpdateSlider()
    {
        ammoMaxSelectAmount = GameManager.GM.sparePartAmount / 2;
        ammoSelect.maxValue = ammoMaxSelectAmount;
        ammoSelect.value = 0;
    }

    public void UpdateSliderTextVal()
    {
        sliderValT.text = ammoSelect.value.ToString();
    }

    public void CreateAmmo()
    {
        GameManager.GM.sparePartAmount -= (int)ammoSelect.value * 2;
        FindObjectOfType<Movement>().GetComponent<Gun>().ammoAmount += (int)ammoSelect.value;

        TextLog.PrintToLog((int)ammoSelect.value + "x ammo created.");
        UpdateSlider();
        UpdateCraftingTexts();
    }

    public void UpgradeWeapon()
    {
        Gun g = FindObjectOfType<Movement>().GetComponent<Gun>();

        if (GameManager.GM.sparePartAmount < g.upgradeCost)
            return;

        if (g.upgradeLvl > 4)
            return;

        switch (g.upgradeLvl)
        {
            case 1:
                g.gName = "Refined Scrapgun";
                g.magazineAmount += 2;
                g.upgradeLvl++;
                break;
            case 2:
                g.gName = "Quickdraw Scrapgun";
                g.reloadTime -= 1;
                g.bulletSpeed += 200;
                g.upgradeLvl++;
                break;
            case 3:
                g.gName = "Deadly Scrapgun";
                g.dmg += 1;
                g.upgradeLvl++;
                break;
            case 4:
                g.gName = "Masterworked Scrapgun";
                g.magazineAmount += 3;
                g.dmg += 1;
                g.bulletSpeed += 350;
                g.reloadTime -= 1;
                g.upgradeLvl++;
                break;
        }

        TextLog.PrintToLog("Upgrade completed.");
        //FindObjectOfType<Movement>().GetComponent<Gun>() = g;
        GameManager.GM.sparePartAmount -= g.upgradeCost;
        UpdateSlider();
        UpdateCraftingTexts();
    }

    public void CreateBattery()
    {
        if (GameManager.GM.sparePartAmount < 20)
            return;

        GameManager.GM.sparePartAmount -= 20;
        GameManager.GM.batteryAmount++;

        TextLog.PrintToLog("Battery created.");
        UpdateSlider();
        UpdateCraftingTexts();
    }

    public void CreateTank()
    {
        if (GameManager.GM.sparePartAmount < 25)
            return;

        GameManager.GM.sparePartAmount -= 25;
        GameManager.GM.emptyTankAmount++;

        TextLog.PrintToLog("Backup tank created.");
        UpdateSlider();
        UpdateCraftingTexts();
    }

    public void CreateHeal()
    {
        if (GameManager.GM.sparePartAmount < 15)
            return;

        GameManager.GM.sparePartAmount -= 15;
        GameManager.GM.healKitAmount++;

        TextLog.PrintToLog("Field repair kit created.");
        UpdateSlider();
        UpdateCraftingTexts();
    }
}
