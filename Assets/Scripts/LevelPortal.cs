﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelPortal : MonoBehaviour
{
    public string lvlToName;
    public string lvlCompleteString;

    public GameObject toNextLvlUI;
    public Text completeText;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Movement>())
        {
            Time.timeScale = 0;
            ConstructLvlString();
            toNextLvlUI.SetActive(true);
            completeText.text = lvlCompleteString;
        }
    }

    public void ConstructLvlString()
    {
        int enemyDefeatedA = GameManager.GM.enemyAmountInLvl - FindObjectsOfType<Enemy>().Length;

        lvlCompleteString += "Collectibles " + GameManager.collectibles.Count + "/" + GameManager.GM.collectibleAmountInLvl + "\n";
        lvlCompleteString += "Health " + FindObjectOfType<Movement>().health + "/" + 3 + "\n";
        lvlCompleteString += "Enemies defeated " + enemyDefeatedA + "/" + GameManager.GM.enemyAmountInLvl + "\n";
        lvlCompleteString += "Weapon upgrade lvl " + FindObjectOfType<Movement>().GetComponent<Gun>().upgradeLvl + "\n";
        lvlCompleteString += "\n";
        lvlCompleteString += "Time " + GameManager.GM.timePassed.ToString("F2") + "\n";
        lvlCompleteString += "Air consumed " + GameManager.GM.airConsumed.ToString("F2") + "\n";
        lvlCompleteString += "Energy consumed " + (GameManager.GM.timePassed / 5).ToString("F2") + "\n";
        lvlCompleteString += "Spare parts collected " + GameManager.GM.sparePartsCollected + "\n";
    }

    public void ToNextLvl()
    {
        Application.LoadLevel(lvlToName);
    }
}
