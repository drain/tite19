﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTile : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<Movement>())
        {
            Death();
        }
        else if(collision.GetComponent<Enemy>())
        {
            Destroy(collision.gameObject);
        }
    }

    private void Death()
    {
        FindObjectOfType<Movement>().StartCoroutine("waitDeath");
    }
}
