﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager GM;

    public StartPos startPosManager;
    public Transform lastPos;

    public GameObject playerPrefab;
    public CameraFollow playerCamera;

    public List<Vector3> waterLvlPos = new List<Vector3>();
    public List<Transform> doorList = new List<Transform>();

    public GameObject waterObject;

    public float airAmount;
    public float maxAirAmount;
    public float energyAmount;
    public int sparePartAmount;

    public int batteryAmount;
    public int emptyTankAmount;
    public int airTankAmount;
    public int healKitAmount;

    public Text health;
    public Text airT;
    public Text energyT;
    public Text sparePartT;

    public Text gName;
    public Text curA;
    public Text maxA;
    public Text resA;

    public GameObject dmgFlash;
    public GameObject deathScreen;

    private Movement player;

    public int collectiblesCollected;
    public int collectibleAmountInLvl;
    public int enemyAmountInLvl;
    public float timePassed;
    public float airConsumed;
    public int sparePartsCollected;

    //TODO: Add mechanics that reduce air and energy, create spare part usage later

    public static List<Collectible> collectibles = new List<Collectible>();

    private void Awake()
    {
        Time.timeScale = 1;
        GM = this;
    }

    private void Start()
    {
        if (lastPos == null)
            lastPos = startPosManager.startPositions[0];
        SpawnPlayer();
        player = FindObjectOfType<Movement>();
        enemyAmountInLvl = FindObjectsOfType<Enemy>().Length;
    }

    public void Update()
    {
        //TODO: take this away from update;
        health.text = "Health: " + player.health;
        airT.text = "Air: " + (int)airAmount;
        energyT.text = "Energy: " + (int)energyAmount;
        sparePartT.text = "Spare Parts: " + sparePartAmount;

        gName.text = player.GetComponent<Gun>().gName;
        curA.text = player.GetComponent<Gun>().currentMagAmount.ToString();
        maxA.text = player.GetComponent<Gun>().magazineAmount.ToString();
        resA.text = player.GetComponent<Gun>().ammoAmount.ToString();

        if(moveWaterLvl)
        {
            UpdateWaterLvl(desiredWaterLvl);
        }

        if (Input.GetKeyDown(KeyCode.B) && batteryAmount > 0)
            UseBattery();

        if (Input.GetKeyDown(KeyCode.F) && airTankAmount > 0)
            UseAirTank();

        if (Input.GetKeyDown(KeyCode.H) && healKitAmount > 0)
            UseHeal();

        timePassed += Time.deltaTime;
    }

    private void SpawnPlayer()
    {
        GameObject p = Instantiate(playerPrefab, lastPos.position, Quaternion.identity);
        CameraFollow c = Instantiate(playerCamera, lastPos.position - Vector3.forward, Quaternion.identity);
        c.target = p;
    }

    public void OpenDoor(int index)
    {
        Transform tempDoor = doorList[index];
        doorList.Remove(doorList[index]);
        Destroy(tempDoor.gameObject);
    }

    private bool moveWaterLvl;
    private int desiredWaterLvl;

    public void ChangeWaterLevel(int lvl)
    {
        desiredWaterLvl = lvl;
        moveWaterLvl = true;
    }

    public void UpdateWaterLvl(int lvl)
    {
        waterObject.transform.position = Vector3.MoveTowards(waterObject.transform.position, waterLvlPos[lvl], Time.deltaTime * 2);
        if(waterObject.transform.position == waterLvlPos[lvl])
        {
            moveWaterLvl = false;
        }
    }

    public void UseBattery()
    {
        if (batteryAmount > 0)
        {
            batteryAmount--;
            energyAmount += 20;

            TextLog.PrintToLog("Used battery, energy increased by " + 20);
            TextLog.PrintToLog(batteryAmount + "x batteries left in reserves.");
        }
    }

    public void UseAirTank()
    {
        if(airTankAmount > 0)
        {
            airTankAmount--;
            emptyTankAmount++;
            airAmount += 30;
            if (airAmount > maxAirAmount)
                airAmount = maxAirAmount;

            TextLog.PrintToLog("Filled airtank with backup: " + 30);
            TextLog.PrintToLog(airTankAmount + "x tanks with air left in reserves.");
        }
    }

    public void UseHeal()
    {
        Movement m = FindObjectOfType<Movement>();

        if (healKitAmount > 0 && m.health < m.maxHealth)
        {
            healKitAmount--;
            FindObjectOfType<Movement>().health++;

            TextLog.PrintToLog("Used field repair kit.");
            TextLog.PrintToLog(healKitAmount + "x kits left in reserves.");
        }
    }
}
