﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public string cName;
    public string lore;
    public Sprite image;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<Movement>())
        {
            if(ContainsCollectible(this))
            {
                TextLog.PrintToLog("Already added to journal.");
                Destroy(this.gameObject);
            }
            else
            {
                GameManager.collectibles.Add(this);
                GameManager.GM.collectiblesCollected++;
                TextLog.PrintToLog("Added lore entry: " + cName + " to journal. Open Collectible tab to read it.");
                Destroy(this.gameObject);
            }
        }
    }

    private bool ContainsCollectible(Collectible c)
    {
        for (int i = 0; i < GameManager.collectibles.Count; i++)
        {
            if (GameManager.collectibles[i].cName == c.cName)
            {
                return true;
            }
        }

        return false;
    }
}
