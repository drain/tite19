﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int dmg;
    public Transform owner;
    public float forceAmount;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform != owner && collision.gameObject.layer != 18)
        {
            if(collision.GetComponent<Enemy>())
            {
                //collision.GetComponent<Enemy>().health -= dmg;
                collision.GetComponent<Enemy>().TakeDmg(dmg, forceAmount);
            }
            else if(collision.GetComponent<Movement>())
            {
                collision.GetComponent<Movement>().TakeDmg(dmg, forceAmount);
            }

            Destroy(gameObject);
        }
    }
}
