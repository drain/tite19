﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SimpleSpriteAnim : MonoBehaviour
{
    public Sprite[] sprites;
    private SpriteRenderer Renderer;
    private float timer = 0;
    private float maxTimer = 0;
    public float speed = 0;


    void Start()
    {
        Renderer = GetComponent<SpriteRenderer>();
        maxTimer = sprites.Length;
    }
    void ChangeSprite(Sprite sprite)
    {
        Renderer.sprite = sprite;
    }

    void Update()
    {
        if (timer >= maxTimer)
        {
            timer = 0;
        }

        timer += Time.deltaTime * speed;

        for (int i = 0; i < sprites.Length; i++)
        {
            if ((int)timer == i + 1)
            {
                ChangeSprite(sprites[i]);
            }
        }
    }
}