﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceObject : MonoBehaviour
{
    public string cName;
    public Sprite image;
    public List<Resource> Resources = new List<Resource>();

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<Movement>())
        {
            //.Log("Found " + name + " you are able to extract materials from it.");
            TextLog.PrintToLog("You found " + cName + " you are able to extract materials from it.");
            //TODO: UI confirmation

            for(int i = 0; i < Resources.Count; i++)
            {
                switch(Resources[i].resourceType)
                {
                    case ResourceType.Air:
                        GameManager.GM.airAmount += Resources[i].amount;
                        TextLog.PrintToLog("Air +" + Resources[i].amount + ".");
                        break;
                    case ResourceType.Energy:
                        GameManager.GM.energyAmount += Resources[i].amount;
                        TextLog.PrintToLog("Energy +" + Resources[i].amount + ".");
                        break;
                    case ResourceType.SpareParts:
                        GameManager.GM.sparePartAmount += Resources[i].amount;
                        GameManager.GM.sparePartsCollected += Resources[i].amount;
                        TextLog.PrintToLog("Spare Parts +" + Resources[i].amount + ".");
                        break;
                }
            }
            Destroy(this.gameObject);
        }
    }
}

[System.Serializable]
public class Resource
{
    public ResourceType resourceType;
    public int amount;
}

[System.Serializable]
public enum ResourceType
{
    Air,
    Energy,
    SpareParts
}
